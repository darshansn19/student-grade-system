package org.training.studentgradesystem.exception;

public class SubjectNotBelongToThisDepatment extends RuntimeException {

	public SubjectNotBelongToThisDepatment() {
		super();
	}

	public SubjectNotBelongToThisDepatment(String message) {
		super(message);
	}

}
