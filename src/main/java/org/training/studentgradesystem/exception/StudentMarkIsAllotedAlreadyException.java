package org.training.studentgradesystem.exception;

public class StudentMarkIsAllotedAlreadyException extends RuntimeException {

	

	public StudentMarkIsAllotedAlreadyException(String message) {
		super(message);
	}
	
	}
