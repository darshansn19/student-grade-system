package org.training.studentgradesystem.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.training.studentgradesystem.response.ApiResponse;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	protected ResponseEntity<Object> handleMethodArgumentsNotValid(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}

	@ExceptionHandler(StudentDepartmentMissMatchException.class)
	protected ResponseEntity<ApiResponse> handleStudentDepartmentMissMatchException(
			StudentDepartmentMissMatchException exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ApiResponse(exception.getMessage(), HttpStatus.CONFLICT));

	}

	@ExceptionHandler(StudentMarkIsAllotedAlreadyException.class)
	protected ResponseEntity<ApiResponse> handleStudentMarkIsAllotedAlreadyException(
			StudentMarkIsAllotedAlreadyException exception) {
		return ResponseEntity.status(HttpStatus.ALREADY_REPORTED)
				.body(new ApiResponse(exception.getMessage(), HttpStatus.CONFLICT));

	}

	@ExceptionHandler(StudnetNotFoundException.class)
	protected ResponseEntity<ApiResponse> handleStudnetNotFoundException(StudnetNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ApiResponse(exception.getMessage(), HttpStatus.NOT_FOUND));

	}
	
	@ExceptionHandler(DepartmentNotFoundException.class)
	protected ResponseEntity<ApiResponse> handleDepartmentNotFoundException(DepartmentNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.CONFLICT)
				.body(new ApiResponse(exception.getMessage(), HttpStatus.NOT_FOUND));

	}

}
