package org.training.studentgradesystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.training.studentgradesystem.entity.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

	


}
