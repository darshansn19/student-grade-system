package org.training.studentgradesystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.training.studentgradesystem.entity.Semester;

@Repository
public interface SemesterRepository extends JpaRepository<Semester, Long>{

	

}
