package org.training.studentgradesystem.repository;

import java.time.Year;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.MarkSheet;
import org.training.studentgradesystem.entity.Student;

@Repository
public interface MarkSheetRepository extends JpaRepository<MarkSheet, Long> {

	List<MarkSheet> findByStudentAndDepartmentAndSemAndYear(Student student, Department department, long semNumber,
			Year year);

}
