package org.training.studentgradesystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentgradesystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentgradesystemApplication.class, args);
	}

}
