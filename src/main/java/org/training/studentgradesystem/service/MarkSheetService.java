package org.training.studentgradesystem.service;

import org.training.studentgradesystem.dto.MarkSheetDto;
import org.training.studentgradesystem.response.ApiResponse;

public interface MarkSheetService {

	ApiResponse studentMarkAllotment(MarkSheetDto markSheetDto);
}
