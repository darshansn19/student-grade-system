package org.training.studentgradesystem.service;

import org.training.studentgradesystem.dto.StudentDto;
import org.training.studentgradesystem.response.ApiResponse;


public interface StudentService {

	ApiResponse enrollStudent(StudentDto dto);

}
