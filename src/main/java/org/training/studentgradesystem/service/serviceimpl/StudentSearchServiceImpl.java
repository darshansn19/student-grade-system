package org.training.studentgradesystem.service.serviceimpl;

import java.time.Year;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.training.studentgradesystem.dto.SearchStudentDto;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.MarkSheet;
import org.training.studentgradesystem.entity.Student;
import org.training.studentgradesystem.exception.DepartmentNotFoundException;
import org.training.studentgradesystem.exception.StudentDepartmentMissMatchException;
import org.training.studentgradesystem.exception.StudnetNotFoundException;
import org.training.studentgradesystem.repository.DepartmentRepository;
import org.training.studentgradesystem.repository.MarkSheetRepository;
import org.training.studentgradesystem.repository.StudentRepository;
import org.training.studentgradesystem.service.StudentSearchService;

@Service
public class StudentSearchServiceImpl implements StudentSearchService {

	private final StudentRepository studentRepository;

	private final DepartmentRepository departmentRepository;
	
	private final MarkSheetRepository markSheetRepository;

	@Autowired
	public StudentSearchServiceImpl(StudentRepository studentRepository, DepartmentRepository departmentRepository,
			MarkSheetRepository markSheetRepository) {
		this.studentRepository = studentRepository;
		this.departmentRepository = departmentRepository;
		this.markSheetRepository = markSheetRepository;
	}

	

	public static final Logger LOGGER = LoggerFactory.getLogger(StudentSearchServiceImpl.class);

	public SearchStudentDto searchStudent(long stuId, long depId, long semNumber, String year) {

		Optional<Student> student = studentRepository.findById(stuId);
		if (student.isEmpty()) {

			LOGGER.error("student not found");
			throw new StudnetNotFoundException("Student Not found");
		}
		Optional<Department> department = departmentRepository.findById(depId);
		if (department.isEmpty()) {
			LOGGER.warn("entered department is not found");
			throw new DepartmentNotFoundException("department Not found");
		}
		if (student.get().getDepartment().getDepId() != (department.get().getDepId())) {
			LOGGER.error("The above student is not present in the particular department");
			throw new StudentDepartmentMissMatchException("Student Not Belong to particular Department");

		}

		List<MarkSheet> markSheets = markSheetRepository.findByStudentAndDepartmentAndSemAndYear(student.get(),
				department.get(), semNumber, Year.parse(year));
		
		HashMap<Long, Long> studentTotal = new HashMap<>();

		for (MarkSheet markSheet : markSheets) {

			studentTotal.merge(markSheet.getStudent().getStuId(), markSheet.getMarks(), Long::sum);
		}
		SearchStudentDto searchStudentDto = new SearchStudentDto();
		searchStudentDto.setStuId(stuId);
		searchStudentDto.setDepId(depId);
		searchStudentDto.setSemNumber(semNumber);
		searchStudentDto.setTotalscore(studentTotal);

		LOGGER.info("Student with total score for a department feched");
		return searchStudentDto;
	}

}
