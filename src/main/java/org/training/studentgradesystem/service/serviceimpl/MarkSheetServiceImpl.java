package org.training.studentgradesystem.service.serviceimpl;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.training.studentgradesystem.dto.MarkSheetDto;
import org.training.studentgradesystem.dto.SubjectMarkDto;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.MarkSheet;
import org.training.studentgradesystem.entity.Student;
import org.training.studentgradesystem.entity.Subject;
import org.training.studentgradesystem.exception.StudentDepartmentMissMatchException;
import org.training.studentgradesystem.repository.DepartmentRepository;
import org.training.studentgradesystem.repository.MarkSheetRepository;
import org.training.studentgradesystem.repository.StudentRepository;
import org.training.studentgradesystem.repository.SubjectRepository;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.MarkSheetService;

@Service
public class MarkSheetServiceImpl implements MarkSheetService {

	private final StudentRepository studentRepository;

	private final DepartmentRepository departmentRepository;

	private final SubjectRepository subjectRepository;

	private final MarkSheetRepository markSheetRepository;

	public static final Logger LOGGER = LoggerFactory.getLogger(MarkSheetServiceImpl.class);

	@Autowired
	public MarkSheetServiceImpl(StudentRepository studentRepository, DepartmentRepository departmentRepository,
			SubjectRepository subjectRepository, MarkSheetRepository markSheetRepository) {

		this.studentRepository = studentRepository;
		this.departmentRepository = departmentRepository;
		this.subjectRepository = subjectRepository;
		this.markSheetRepository = markSheetRepository;
	}

	@Override
	public ApiResponse studentMarkAllotment(MarkSheetDto markSheetDto) {

		Optional<Student> student = studentRepository.findById(markSheetDto.getStuId());
		if (student.isEmpty()) {
			LOGGER.warn("Entered Student is not found");
			return new ApiResponse("Student not found", HttpStatus.NOT_FOUND);
		}

		Optional<Department> department = departmentRepository.findById(markSheetDto.getDepId());
		if (department.isEmpty()) {
			LOGGER.warn("entered department is not found");
			return new ApiResponse("department not found", HttpStatus.NOT_FOUND);
		}
		if (student.get().getDepartment().getDepId() != (department.get().getDepId())) {
			LOGGER.error("The above student is not present in the particular department");
			throw new StudentDepartmentMissMatchException("studdent and department missmatch");

		}

		List<MarkSheet> markSheets = new ArrayList<>();

		for (SubjectMarkDto subjectMarkDto : markSheetDto.getSubjectMarkDto()) {
			MarkSheet newmarkSheet = new MarkSheet();
			newmarkSheet.setStudent(student.get());
			newmarkSheet.setDepartment(department.get());
			Optional<Subject> subject = subjectRepository.findById(subjectMarkDto.getSubId());
			if (subject.isEmpty()) {
				LOGGER.warn("Subject not found");
				return new ApiResponse("subject not found", HttpStatus.NOT_FOUND);
			}
			newmarkSheet.setSubject(subject.get());
			newmarkSheet.setSem(markSheetDto.getSemName());
			newmarkSheet.setMarks(subjectMarkDto.getMarks());
			newmarkSheet.setYear(Year.parse(markSheetDto.getYear()));
			markSheets.add(newmarkSheet);
		}

		LOGGER.info("mark alloted to the particular student");
		markSheetRepository.saveAll(markSheets);
		return new ApiResponse("mark allotment succesfull", HttpStatus.CREATED);
	}

}
