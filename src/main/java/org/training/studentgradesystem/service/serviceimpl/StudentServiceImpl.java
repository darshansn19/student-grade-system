package org.training.studentgradesystem.service.serviceimpl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.training.studentgradesystem.dto.StudentDto;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.Student;
import org.training.studentgradesystem.repository.DepartmentRepository;
import org.training.studentgradesystem.repository.StudentRepository;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	private final DepartmentRepository departmentRepository;

	private final StudentRepository repository;

	@Autowired
	public StudentServiceImpl(DepartmentRepository departmentRepository, StudentRepository repository) {
		
		this.departmentRepository = departmentRepository;
		this.repository = repository;
	}

	public static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Override
	public ApiResponse enrollStudent(StudentDto dto) {

		if (dto == null) {
			LOGGER.warn("provide Student Information");
			return new ApiResponse("provide student information", HttpStatus.BAD_REQUEST);
		}

		Optional<Student> student2 = repository.findByMailId(dto.getMailId());
		if (student2.isPresent()) {
			LOGGER.warn("The above Student is already present");
			return new ApiResponse("student already enrolled", HttpStatus.ALREADY_REPORTED);
		}

		Optional<Department> departmentCheck = departmentRepository.findByDepName(dto.getDepName());
		if (departmentCheck.isEmpty()) {
			LOGGER.warn("Please change the department");
			return new ApiResponse("please change the department", HttpStatus.OK);
		} else {
			Student student = new Student();
			student.setStuName(dto.getStuName());
			student.setMailId(dto.getMailId());
			student.setDepartment(departmentCheck.get());
			repository.save(student);
			LOGGER.info("The above student is Enrolled");
			return new ApiResponse("student Enrolled", HttpStatus.CREATED);

		}

	}

}
