package org.training.studentgradesystem.dto;

import java.util.HashMap;

import lombok.Data;

@Data
public class SearchStudentDto {
	
	private long stuId;
	private long depId;
	private long semNumber;
	private HashMap<Long, Long> totalscore;

}
