package org.training.studentgradesystem.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Valid
public class StudentDto {

	@NotNull(message = "enter the subject")
	private String stuName;
	
	@NotNull(message="enter the valid mail Id")
	@Email
	private String mailId;
	
	@NotNull(message = "enter the Department name")
	private String depName;
	
}
