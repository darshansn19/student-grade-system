package org.training.studentgradesystem.dto;

import java.util.List;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MarkSheetDto {

	@NotNull(message = "enter the student Id")
	private long stuId;

	@NotNull(message = "enter the Department Id")
	private long depId;

	@NotEmpty(message = "enter the Sem Number")
	private long semName;
	
	private List<SubjectMarkDto> subjectMarkDto;



	@NotNull(message = "enter the sem year")
	private String year;
}
