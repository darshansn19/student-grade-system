package org.training.studentgradesystem.entity;

import java.time.Year;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.Data;


@Data
@Entity
public class MarkSheet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long markSheetId;
	
	@ManyToOne
	private Student student;
	
	@ManyToOne
	private  Department department;
	
	@ManyToOne
	private Subject subject;
	
	private  long sem;
	 
	private  long marks;
	
	private Year year;
	
	
}
