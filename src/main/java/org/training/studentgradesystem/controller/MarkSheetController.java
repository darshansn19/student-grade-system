package org.training.studentgradesystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.studentgradesystem.dto.MarkSheetDto;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.MarkSheetServiceImpl;

import jakarta.validation.Valid;

@RestController
public class MarkSheetController {

	private final MarkSheetServiceImpl markSheetServiceImpl;

	@Autowired
	public MarkSheetController(MarkSheetServiceImpl markSheetServiceImpl) {

		this.markSheetServiceImpl = markSheetServiceImpl;
	}

	@PostMapping(value = "/students/marks")
	public ResponseEntity<ApiResponse> studentMarkAllotment(@Valid @RequestBody MarkSheetDto markSheetDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(markSheetServiceImpl.studentMarkAllotment(markSheetDto));
	}
}
