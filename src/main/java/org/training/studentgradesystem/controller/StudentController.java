package org.training.studentgradesystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.studentgradesystem.dto.StudentDto;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.StudentServiceImpl;

import jakarta.validation.Valid;

@RestController
public class StudentController {

	private final StudentServiceImpl studentServiceImpl;

	@Autowired
	public StudentController(StudentServiceImpl studentServiceImpl) {

		this.studentServiceImpl = studentServiceImpl;
	}

	@PostMapping(value = "/students")
	public ResponseEntity<ApiResponse> enrollStudent(@Valid @RequestBody StudentDto dto) {

		return ResponseEntity.status(HttpStatus.CREATED).body(studentServiceImpl.enrollStudent(dto));

	}

}
