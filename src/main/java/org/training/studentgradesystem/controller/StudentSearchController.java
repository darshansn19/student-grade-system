package org.training.studentgradesystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.training.studentgradesystem.dto.SearchStudentDto;
import org.training.studentgradesystem.service.serviceimpl.StudentSearchServiceImpl;

import jakarta.validation.Valid;

@RestController
public class StudentSearchController {

	private final StudentSearchServiceImpl studentSearchServiceImpl;

	@Autowired
	public StudentSearchController(StudentSearchServiceImpl studentSearchServiceImpl) {

		this.studentSearchServiceImpl = studentSearchServiceImpl;
	}

	@GetMapping(value = "/students/marks")
	public ResponseEntity<SearchStudentDto> serachStudent(@Valid @RequestParam long stuId, long depId, long semNumber,
			String year) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(studentSearchServiceImpl.searchStudent(stuId, depId, semNumber, year));

	}

}
