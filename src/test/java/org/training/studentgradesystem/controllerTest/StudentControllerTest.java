package org.training.studentgradesystem.controllerTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.studentgradesystem.controller.StudentController;
import org.training.studentgradesystem.dto.StudentDto;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.StudentServiceImpl;

@ExtendWith(SpringExtension.class)
class StudentControllerTest {

	@Mock
	StudentServiceImpl studentServiceImpl;

	@InjectMocks
	StudentController studentController;

	@Test
	void testEnrollStudentCheck() {
		StudentDto dto = new StudentDto();
		dto.setStuName("darshan");
		dto.setMailId("darshan@gmail.com");
		dto.setDepName("EC");
		
		

		ApiResponse apiResponse = new ApiResponse();
		apiResponse.setHttpStatus(HttpStatus.CREATED);

		Mockito.when(studentServiceImpl.enrollStudent(dto)).thenReturn(apiResponse);

		ResponseEntity<ApiResponse> result = studentController.enrollStudent(dto);

		Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
	}
}
