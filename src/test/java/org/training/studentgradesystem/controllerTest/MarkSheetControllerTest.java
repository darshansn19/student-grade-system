package org.training.studentgradesystem.controllerTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.studentgradesystem.controller.MarkSheetController;
import org.training.studentgradesystem.dto.MarkSheetDto;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.MarkSheetServiceImpl;


@ExtendWith(SpringExtension.class)
 class MarkSheetControllerTest {

	@Mock
	MarkSheetServiceImpl markSheetServiceImpl;
	
	@InjectMocks
	MarkSheetController markSheetController;
	
	@Test
	void testMarkSheetController()
	{
		ApiResponse apiResponse=new ApiResponse();
		apiResponse.setHttpStatus(HttpStatus.CREATED);
		
		
		
		MarkSheetDto dto=new MarkSheetDto();
		Mockito.when(markSheetServiceImpl.studentMarkAllotment(dto)).thenReturn(apiResponse);
		
		ResponseEntity<ApiResponse> result=markSheetController.studentMarkAllotment(dto);
		Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
			}
}
