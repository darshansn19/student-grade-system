package org.training.studentgradesystem.serviceImplTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.studentgradesystem.dto.StudentDto;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.Student;
import org.training.studentgradesystem.repository.DepartmentRepository;
import org.training.studentgradesystem.repository.StudentRepository;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.StudentServiceImpl;

@ExtendWith(SpringExtension.class)
class StudentServiceImplTest {
	

	@Mock
	StudentRepository repository;

	@Mock
	DepartmentRepository departmentRepository;

	@InjectMocks
	StudentServiceImpl studentServiceImpl;

	@Test
	void  testEnrollStudentSuccess() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		StudentDto dto = new StudentDto();
		dto.setDepName("EC");
		dto.setMailId("darshan@gmail.com");
		dto.setStuName("darshan");

		Mockito.when(repository.findByMailId(anyString())).thenReturn(Optional.empty());
		Mockito.when(departmentRepository.findByDepName(anyString())).thenReturn(Optional.of(department));
		Mockito.when(repository.save(student)).thenReturn(student);

		ApiResponse apiResponse = studentServiceImpl.enrollStudent(dto);
		assertEquals("student Enrolled", apiResponse.getMessage());

	}

	@Test
	void testEnrollStudentNullcheck() {

		StudentDto dto = null;
		ApiResponse apiResponse = studentServiceImpl.enrollStudent(dto);
		assertEquals("provide student information", apiResponse.getMessage());

	}

	@Test
	void testEnrollStudentCheckStudentIsPresent() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		StudentDto dto = new StudentDto();
		dto.setDepName("EC");
		dto.setMailId("darshan@gmail.com");
		dto.setStuName("darshan");

		Mockito.when(repository.findByMailId(anyString())).thenReturn(Optional.of(student));
		ApiResponse apiResponse = studentServiceImpl.enrollStudent(dto);
		assertEquals("student already enrolled", apiResponse.getMessage());
	}
	
	@Test
	void testEnrollStudentCheckDepartmentIsPresent()
	{
		StudentDto dto = new StudentDto();
		dto.setDepName("ME");
		dto.setMailId("darshan@gmail.com");
		dto.setStuName("darshan");
		
		Mockito.when(repository.findByMailId(anyString())).thenReturn(Optional.empty());
		Mockito.when(departmentRepository.findByDepName(anyString())).thenReturn(Optional.empty());
		
		ApiResponse apiResponse = studentServiceImpl.enrollStudent(dto);
		assertEquals("please change the department", apiResponse.getMessage());
	

	}
}
