package org.training.studentgradesystem.serviceImplTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.studentgradesystem.dto.MarkSheetDto;
import org.training.studentgradesystem.dto.SubjectMarkDto;
import org.training.studentgradesystem.entity.Department;
import org.training.studentgradesystem.entity.MarkSheet;
import org.training.studentgradesystem.entity.Student;
import org.training.studentgradesystem.entity.Subject;
import org.training.studentgradesystem.exception.StudentDepartmentMissMatchException;
import org.training.studentgradesystem.repository.DepartmentRepository;
import org.training.studentgradesystem.repository.MarkSheetRepository;
import org.training.studentgradesystem.repository.StudentRepository;
import org.training.studentgradesystem.repository.SubjectRepository;
import org.training.studentgradesystem.response.ApiResponse;
import org.training.studentgradesystem.service.serviceimpl.MarkSheetServiceImpl;

@ExtendWith(SpringExtension.class)
 class MarkSheetServiceImplTest {
	

	@Mock
	StudentRepository studentRepository;

	@Mock
	DepartmentRepository departmentRepository;

	@Mock
	SubjectRepository subjectRepository;

	@Mock
	MarkSheetRepository markSheetRepository;

	@InjectMocks
	MarkSheetServiceImpl markSheetServiceImpl;

	@Test
	void testMarkAllotment() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		List<SubjectMarkDto> subjectList = new ArrayList<>();
		SubjectMarkDto dto1 = new SubjectMarkDto();
		dto1.setSubId(101);
		dto1.setMarks(90);
		SubjectMarkDto dto2 = new SubjectMarkDto();
		dto2.setSubId(102);
		dto2.setMarks(100);
		SubjectMarkDto dto3 = new SubjectMarkDto();
		dto3.setSubId(103);
		dto3.setMarks(60);
		subjectList.add(dto1);
		subjectList.add(dto2);
		subjectList.add(dto3);

		MarkSheetDto markSheetDto = new MarkSheetDto();
		markSheetDto.setStuId(1);
		markSheetDto.setDepId(1);
		markSheetDto.setSemName(1);
		markSheetDto.setYear("2023");
		markSheetDto.setSubjectMarkDto(subjectList);

		Subject subject = new Subject();
		subject.setSubId(101);
		subject.setSubName("verlog");
		subject.setDepartment(department);

		Subject subject1 = new Subject();
		subject.setSubId(102);
		subject.setSubName("verlog");
		subject.setDepartment(department);

		Subject subject2 = new Subject();
		subject.setSubId(103);
		subject.setSubName("verlog");
		subject.setDepartment(department);

		List<MarkSheet> markSheets = new ArrayList<>();
		MarkSheet markSheet1 = new MarkSheet();
		markSheet1.setDepartment(department);
		markSheet1.setStudent(student);
		markSheet1.setMarks(90);
		markSheet1.setMarkSheetId(1);
		markSheet1.setSem(1);
		markSheet1.setYear(Year.parse("2023"));
		markSheet1.setSubject(subject1);

		MarkSheet markSheet2 = new MarkSheet();
		markSheet2.setDepartment(department);
		markSheet2.setStudent(student);
		markSheet2.setMarks(90);
		markSheet2.setMarkSheetId(2);
		markSheet2.setSem(1);
		markSheet2.setYear(Year.parse("2023"));
		markSheet2.setSubject(subject2);
		markSheets.add(markSheet1);
		markSheets.add(markSheet2);

		Mockito.when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student));
		Mockito.when(departmentRepository.findById(anyLong())).thenReturn(Optional.of(department));
		Mockito.when(subjectRepository.findById(anyLong())).thenReturn(Optional.of(subject));
		Mockito.when(markSheetRepository.saveAll(anyList())).thenReturn(markSheets);

		ApiResponse apiResponse = markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		assertEquals("mark allotment succesfull", apiResponse.getMessage());

	}

	@Test
	void testStudentNotFoundCheck() {
		List<SubjectMarkDto> subjectList = new ArrayList<>();
		SubjectMarkDto dto1 = new SubjectMarkDto();
		dto1.setSubId(101);
		dto1.setMarks(90);
		SubjectMarkDto dto2 = new SubjectMarkDto();
		dto2.setSubId(102);
		dto2.setMarks(100);
		SubjectMarkDto dto3 = new SubjectMarkDto();
		dto3.setSubId(103);
		dto3.setMarks(60);
		subjectList.add(dto1);
		subjectList.add(dto2);
		subjectList.add(dto3);

		MarkSheetDto markSheetDto = new MarkSheetDto();
		markSheetDto.setStuId(1);
		markSheetDto.setDepId(1);
		markSheetDto.setSemName(1);
		markSheetDto.setYear("2023");
		markSheetDto.setSubjectMarkDto(subjectList);

		Mockito.when(studentRepository.findById(anyLong())).thenReturn(Optional.empty());
		ApiResponse apiResponse = markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		assertEquals("Student not found", apiResponse.getMessage());

	}

	@Test
	void testDepartmentNotFoundCheck() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		List<SubjectMarkDto> subjectList = new ArrayList<>();
		SubjectMarkDto dto1 = new SubjectMarkDto();
		dto1.setSubId(101);
		dto1.setMarks(90);
		SubjectMarkDto dto2 = new SubjectMarkDto();
		dto2.setSubId(102);
		dto2.setMarks(100);
		SubjectMarkDto dto3 = new SubjectMarkDto();
		dto3.setSubId(103);
		dto3.setMarks(60);
		subjectList.add(dto1);
		subjectList.add(dto2);
		subjectList.add(dto3);

		MarkSheetDto markSheetDto = new MarkSheetDto();
		markSheetDto.setStuId(1);
		markSheetDto.setDepId(1);
		markSheetDto.setSemName(1);
		markSheetDto.setYear("2023");
		markSheetDto.setSubjectMarkDto(subjectList);

		Mockito.when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student));
		Mockito.when(departmentRepository.findById(anyLong())).thenReturn(Optional.empty());
		ApiResponse apiResponse = markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		assertEquals("department not found", apiResponse.getMessage());

	}

	@Test
	void testStudentDepartmentMissMatchCheck() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		Department department1 = new Department();
		department.setDepId(102);
		department.setDepName("EC");
		student.setDepartment(department);

		List<SubjectMarkDto> subjectList = new ArrayList<>();
		SubjectMarkDto dto1 = new SubjectMarkDto();
		dto1.setSubId(101);
		dto1.setMarks(90);
		SubjectMarkDto dto2 = new SubjectMarkDto();
		dto2.setSubId(102);
		dto2.setMarks(100);
		SubjectMarkDto dto3 = new SubjectMarkDto();
		dto3.setSubId(103);
		dto3.setMarks(60);
		subjectList.add(dto1);
		subjectList.add(dto2);
		subjectList.add(dto3);

		MarkSheetDto markSheetDto = new MarkSheetDto();
		markSheetDto.setStuId(1);
		markSheetDto.setDepId(102);
		markSheetDto.setSemName(1);
		markSheetDto.setYear("2023");
		markSheetDto.setSubjectMarkDto(subjectList);

		Mockito.when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student));
		Mockito.when(departmentRepository.findById(anyLong())).thenReturn(Optional.of(department1));

		Assertions.assertThrows(StudentDepartmentMissMatchException.class, () -> {
			markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		});
	}

	@Test
	void testSubjectNotFoundCheck() {
		Student student = new Student();
		student.setStuId(1);
		student.setStuName("darshan");
		Department department = new Department();
		department.setDepId(101);
		department.setDepName("EC");
		student.setDepartment(department);
		student.setMailId("darshan@gmail.com");

		List<SubjectMarkDto> subjectList = new ArrayList<>();
		SubjectMarkDto dto1 = new SubjectMarkDto();
		dto1.setSubId(101);
		dto1.setMarks(90);
		SubjectMarkDto dto2 = new SubjectMarkDto();
		dto2.setSubId(102);
		dto2.setMarks(100);
		SubjectMarkDto dto3 = new SubjectMarkDto();
		dto3.setSubId(103);
		dto3.setMarks(60);
		subjectList.add(dto1);
		subjectList.add(dto2);
		subjectList.add(dto3);

		MarkSheetDto markSheetDto = new MarkSheetDto();
		markSheetDto.setStuId(1);
		markSheetDto.setDepId(1);
		markSheetDto.setSemName(1);
		markSheetDto.setYear("2023");
		markSheetDto.setSubjectMarkDto(subjectList);

		Mockito.when(studentRepository.findById(anyLong())).thenReturn(Optional.of(student));
		Mockito.when(departmentRepository.findById(anyLong())).thenReturn(Optional.of(department));
		Mockito.when(subjectRepository.findById(anyLong())).thenReturn(Optional.empty());

		ApiResponse apiResponse = markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		assertEquals("subject not found", apiResponse.getMessage());
	}
	
	@Test
	void tesNullCheck()
	{
		MarkSheetDto markSheetDto = null;
		Assertions.assertThrows(NullPointerException.class, () -> {
			markSheetServiceImpl.studentMarkAllotment(markSheetDto);
		});
		
		
	}
}
